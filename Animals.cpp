#include <iostream>

class Animal
{
public: 
    virtual void Voice() 
    {
        std::cout << " ";
    }
};

class Dog : public Animal
{ 
public:
    void Voice() override
    {
        std::cout << "Woof! \n";
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Meow! \n";
    }
};

class Mouse : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Eek! \n";
    }
};

int main()
{
    Animal *animal[3];
    Animal* dog = new Dog;
    Animal* cat = new Cat;
    Animal* mouse = new Mouse;
    animal[0] = dog;
    animal[1] = cat;
    animal[2] = mouse;
    for (int i = 0; i < 3; ++i)
    {
        animal[i]->Voice();
    }
   
}
